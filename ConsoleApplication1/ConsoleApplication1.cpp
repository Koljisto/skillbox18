﻿#include <iostream>
#include <string>

#define floatType // Тут выбираем тип стэка

#ifdef floatType
#define type float
#endif // floatType

#ifdef intType
#define type int
#endif // intType

#ifdef doubleType
#define type double
#endif // doubleType

#ifdef stringType
#define type std::string
#endif // floatType

struct node
{
    type data;
    node* next;
};

class Stack {
public:
    node* top;
    Stack() {};
    void push() {
        node* temp;
        temp = new node;
        std::cout << "Введите элемент :" << std::endl;
        std::cin >> temp->data;
        temp->next = top;
        top = temp;
        std::cout << "Элемент вставлен в стэк:" << temp->data << std::endl;
    }
    void pop() {
        if (top != NULL)
        {
            node* temp = top;
            top = top->next;
            std::cout << temp->data << " удалён.";
            delete temp;
        }
        else {
            std::cout << "Стэк пустой";
        }
    }
    void displayItems() {
        node* temp = top;
        while (temp != NULL)
        {
            std::cout << temp->data << " ";
            temp = temp->next;
        }
        std::cout << ";" << std::endl;

    }
};

void printMenu() {
    std::cout << "Введите номер операции:" << std::endl;
    std::cout << "0 - Выйти;" << std::endl;
    std::cout << "1 - Вставить элемент;" << std::endl;
    std::cout << "2 - Вытащить элемент;" << std::endl;
    std::cout << "3 - Вывести все элементы." << std::endl;
    std::cout << "__________________________" << std::endl;
}

int main()
{
    setlocale(LC_ALL, "Russian");
    Stack stack;
    printMenu();
    int choice, tempBool = false;
    do
    {
        if (tempBool) {
            printMenu();
        } else {
            tempBool = true;
        }
        std::cin >> choice;
        if (choice == 0) {
            break;
        } else if (choice == 1) {
            stack.push();
        } else if (choice == 2) {
            stack.pop();
        } else if (choice == 3) {
            stack.displayItems();
        } else {
            std::cout << "Неверный выбор, попробуй еще раз." << std::endl;
        }
    } while (choice != 0);
}